# README #

This is a legacy repository. The scripts contained here were written long ago for an older version of SRL ([SRL-6](https://github.com/SRL/SRL-6)).

Please refer to [SRL-scripts](https://gitlab.com/KeepBotting/srl-scripts) for all current and future Simba scripts and related development.