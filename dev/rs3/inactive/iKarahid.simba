program iKarahid;
{$define smart}
{$i srl-6/srl.simba}
{$i sps/lib/sps-rs3.simba}
{$define debug_on} //comment this line to disable script debug messages

const //NO TOUCHY
{ crafting }
  crafting = 0;
  necklace = 0;
  bracelet = 1;

{ smelting }
  smelting = 1;
  bronze = 0;
  iron = 1;
  steel = 2;
  mithril = 3;
  adamant = 4;
  rune = 5;
  cannonball = 6;

{ cooking }
  cooking = 2;

{ tanning }
  tanning = 3;
  soft = 0;
  hard = 1;
  green = 2;
  blue = 3;
  red = 4;
  black = 5;

  auto = -1;


///////////////////////////////////Start setting up the script here. Refer to the comments if you don't know what you're doing.
///////////////////////////////////
/////       Start Setup     ///////
///////////////////////////////////
///////////////////////////////////Start setting up the script here. Refer to the comments if you don't know what you're doing.

const
   (* player info *)
   playerNames  = ['bot1']; //Put your player's name (or nickname, if you set one) here.
   playerFile   = 'default'; //Put your playerfile's name here. Default is 'default'.
   desiredWorld = -1; //Enter your desired world number here. 0 for random, -1 for play button.

   (* globals *)

   { Valid inputs: crafting, smelting, cooking, tanning }
   doWhat = crafting;

   { Valid inputs: necklace, bracelet }
   craftWhat = bracelet; { Ignore if not crafting }

   { Valid inputs: bronze, iron, steel, mithril, adamant, rune, cannonball }
   smeltWhat = bronze; { Ignore if not smelting }

   { Valid inputs: none yet }
   cookWhat = 9001; { Ignore if not cooking }

   { Valid inputs: soft, hard, green, blue, red, black }
   tanWhat = soft; { Ignore if not tanning }

   slot1 = 1; //Which bank slot do you have your hides/bars/primary ore in?
   slot2 = 2; //Which bank slot do you have your secondary item (if any) in?

   box = auto; //Which production screen box is your item in? Leave alone to use the default.

   bankPin = '6969'; //What's your bank pin (if any)?

   (* options *)

///////////////////////////////////Don't modify the script ANY FURTHER unless you know what you're doing. You could break stuff!
///////////////////////////////////
/////       Stop Setup      ///////
///////////////////////////////////
///////////////////////////////////Don't modify the script ANY FURTHER unless you know what you're doing. You could break stuff!

type
  colorInfo = record
  cts:TColorSettings;
  col, tol:integer;
  area:TBox;
end;

var
(* colors *)
  furnace, bankers:colorInfo;

(* DTMs *)
  goldBarDtm, goldNecklaceDtm, goldBraceletDtm, genericBarDtm, genericOreDtm:integer;

(* walking *)
  map:TSPSArea;
  paths:T2DPointArray;
  currentLocation:string;

(* antiban *)
  randomDirection:integer;

(* prices *)
  goldBarPrice, goldNecklacePrice, goldBraceletPrice:integer;
  copperOrePrice, tinOrePrice, ironOrePrice, mithrilOrePrice, adamantOrePrice, runeOrePrice, coalPrice:integer;
  bronzeBarPrice, ironBarPrice, steelBarPrice, mithrilBarPrice, adamantBarPrice, runeBarPrice, cannonballPrice:integer;

(* progress report *)
  items, loads, items2, loads2:integer;
  sstatus:string;

///////////////////////////////////
///////////////////////////////////
/////        Debugging      ///////
///////////////////////////////////
///////////////////////////////////

procedure writeDebug(text:string);
begin
{$IFDEF debug_on} writeLn('[DEBUG]     : ' + text); {$ENDIF}
end;

procedure writeWarn(text:string);
begin
{$IFDEF debug_on}
writeln('');
writeLn('[WARNING]   : ' + text);
writeln('');
{$ENDIF}
end;

procedure writeError(text:string);
begin
{$IFDEF debug_on}
writeln('');
writeLn('[ERROR]     : ' + text);
writeln('');
{$ENDIF}
end;

///////////////////////////////////
///////////////////////////////////
/////      Initializing     ///////
///////////////////////////////////
///////////////////////////////////

function getPrice(theItem:string):integer;
var
  thePage, thePrice:string;
  t:TTimeMarker;
begin
 t.start;
 thePage := getPage('http://runescape.wikia.com/wiki/Exchange:' + theItem);
 thePrice := between('GEPrice">', '</span>', thePage);
 thePrice := replace(thePrice, ',', '', [rfReplaceAll]);
 result := strToIntDef(thePrice, 0);
 t.pause;
 theItem := replace(theItem, '_', ' ', [rfReplaceAll]);
 writeDebug('Grabbed the price of ' + theItem + ' (' + thePrice + ') in ' + toStr(t.getTime()) + ' ms.');
 t.reset;
end;

procedure stop(); //hammer time
begin
  writeWarn('*** Terminating script ***');
  if isLoggedIn() then
   writeDebug('Logging out');
  players[currentPlayer].logout();
  writeDebug('Freeing DTMs');
  freeDtm(goldBarDtm);
  freeDtm(goldNecklaceDtm);
  writeWarn('*** Terminated script ***');
end;

procedure verify();
begin
  if not inRange(doWhat, 0, 3) then
   begin
     writeError('doWhat is invalid! Use crafting, smithing, cooking, tanning');
     terminateScript();
   end;

  if not inRange(craftWhat, 0, 1) then
   begin
     writeError('craftWhat is invalid! Use necklace, bracelet');
     terminateScript();
   end;

  if not inRange(smeltWhat, 0, 6) then
   begin
     writeError('smeltWhat is invalid! Use bronze, iron, steel, mithril, adamant, rune, cannonball');
     terminateScript();
   end;

  if not inRange(tanWhat, 0, 5) then
   begin
     writeError('tanWhat is invalid! Use soft, hard, green, blue, red, black');
     terminateScript();
   end;
end;

procedure initScript();
var i:integer;
begin
   clearDebug();
   addOnTerminate('stop');
   smartEnableDrawing := true;
   smartShowConsole := false;
   smartPlugins := ['opengl32.dll', 'd3d9.dll'];
   writeDebug('Spawning SMART client...');
   disableSrlDebug := false;
   setupSrl();
   players.setup(playerNames, playerFile);
   currentPlayer := 0;
   for i := 0 to high(players) do
   begin
      players[i].world := desiredWorld;
      players[i].isActive := true;
      writeDebug('Logging in...');
   end;
    begin
     if (not players[currentPlayer].login()) then
      exit;
     exitTreasure();
     writeDebug('Setup complete - player is logged in.');
    end;
end;

procedure initPrices();
begin
  { crafting prices }
  goldBarPrice := getPrice('Gold_bar');
  goldNecklacePrice := getPrice('Gold_necklace');
  goldBraceletPrice := getPrice('Gold_bracelet');

  { smelting prices }
  copperOrePrice := getPrice('Copper_ore');
  tinOrePrice := getPrice('Tin_ore');
  ironOrePrice := getPrice('Iron_ore');
  mithrilOrePrice := getPrice('Mithril_ore');
  adamantOrePrice := getPrice('Adamantite_ore');
  runeOrePrice := getPrice('Runite_ore');
  coalPrice := getPrice('Coal');

  bronzeBarPrice := getPrice('Bronze_bar');
  ironBarPrice := getPrice('Iron_bar');
  steelBarPrice := getPrice('Steel_bar');
  mithrilBarPrice := getPrice('Mithril_bar');
  adamantBarPrice := getPrice('Adamant_bar');
  runeBarPrice := getPrice('Rune_bar');
  cannonballPrice := getPrice('Cannonball');

  { cooking prices }

  { tanning prices }
end;

procedure initPlayer();
begin
  writeDebug('Setting camera.');
  minimap.clickCompass();
  mainScreen.setAngle(MS_ANGLE_HIGH);

 writeDebug('Setting run.');
  if not (minimap.isRunEnabled()) then
   minimap.toggleRun(true);

 {writeDebug('Setting gametab.');
  if (not gameTabs.isTabActive(TAB_BACKPACK)) then
   gameTabs.openTab(TAB_BACKPACK);}

  writeDebug('Player has been initalized.');
end;

procedure initColors();
begin

  with furnace do
  begin
    cts.cts := 2;
    cts.modifier.hue := 0.52;
    cts.modifier.saturation := 1.66;
    cts.modifier.sensitivity := 15;
    col := 10801377;
    area := mainScreen.getBounds();
    tol := 2;
  end;

  with bankers do
  begin
    cts.cts := 2;
    cts.modifier.hue := 0.50;
    cts.modifier.saturation := 6.34;
    cts.modifier.sensitivity := 15;
    col := 15004149;
    area := mainScreen.getBounds();
    tol := 2;
  end;

 writeDebug('Colors have been initalized.');
end;

procedure initDtms();
begin
  goldNecklaceDtm := dtmFromString('mrAAAAHichcwxCoAgAEbhXycP0NCxA2myzZLCJsHF0/kWR+vBtz4n6bZSxYOIgoSGF5eRdmSciDgQ4JG2lYudWvSd+THqQ6oL9g==');
  goldBarDtm := dtmFromString('mbQAAAHicY2VgYHjJzMBwC4gfA/E7IP4ExIqMDAyaQKwDxBpArALEl5bLAlUzoWARBkzAiAWDAQBl8gcZ');
  goldBraceletDtm := dtmFromString('mggAAAHicY2NgYJjGxMAwEYjnAPEEIJ4JxPOB+DIjA8N1IL4AxGeA+B4Q3wLisxvlgLqYMDAXA3bAiANDAADyWgpY');

  genericBarDtm := dtmFromString('mVAEAAHicpc7NCkBQEIbhOecKrFyujSSKEqWUuDil/BQpYsMrZ2XJV0/N1Hw1logkWiRDgRqlmXOkiBHAgwsfISL9dAcl0qBFjwkLVhzYsZl9xojO3N+9yrH5Qn92yr+on965AOM8Gp0=');
  genericOreDtm := dtmFromString('mwQAAAHic42RgYMhkYmCIAOIYIE4F4lIgLgPiXCBOAuJiID7FyMBwFogvAfENID4BxIeBeC8UnwdiWzdboGlMePF/BsKAkQgMBwAOnwzB');

  writeDebug('DTMs have been initalized.');
end;

procedure initPaths();
begin
  spsAnyAngle := true;
  {map.setup('map', '', __DEFAULT_ACCURACY, __DEFAULT_TOLERANCE, 0.7);}
  map.setup('map', '', 10, 800.0, 0.80);
  writeDebug('Map has been initalized.');

  setLength(paths, 3);
  paths[0] := [Point(123, 230), Point(113, 202)]; //to furnace
  paths[1] := [Point(99, 284), Point(101, 230)]; //to range
  paths[2] := [Point(99, 284), Point(107, 180)]; //to tanner
  paths[3] := [Point(179, 232), Point(120, 240), Point(115, 300), Point(98, 287)]; //init
  writeDebug('Paths have been initalized.');
end;

procedure redraw();
var
  location:TPoint;
  gp, profit:integer;
  secs, gp2, items2, loads2:extended;
  losingMoney:boolean;
begin
  case (doWhat) of
  crafting:
  case (craftWhat) of
  necklace: profit := (goldNecklacePrice - goldBarPrice);
  bracelet: profit := (goldBraceletPrice - goldBarPrice);
  end;
  smelting:
  case (smeltWhat) of
  bronze: profit := (bronzeBarPrice - (tinOrePrice + copperOrePrice));
  iron: profit := (ironBarPrice - ironOrePrice);
  steel: profit := (steelBarPrice - (ironOrePrice + (coalPrice * 2)));
  mithril: profit := (mithrilBarPrice - (mithrilOrePrice + (coalPrice * 4)));
  adamant: profit := (adamantBarPrice - (adamantOrePrice + (coalPrice * 6)));
  rune: profit := (runeBarPrice - (runeOrePrice + (coalPrice * 8)));
  cannonball: profit := ((cannonballPrice * 4) - steelBarPrice);
  end;
  cooking:
  case (cookWhat) of
  end;
  tanning:
  case (tanWhat) of
  end;
  end;

  if (profit < 0) then
   losingMoney := true;

  gp := (items * profit);
  secs := (getTimeRunning() / 1000);
  gp2 := ((gp * 60 * 60) / secs);
  items2 := ((items * 60 * 60) / secs);
  loads2 := ((loads * 60 * 60) / secs);

  clearDebug();
  smartImage.clear();
  smartImage.drawBox(intToBox(chatBox.x1, chatBox.y1, chatBox.x2, chatBox.y2), true, 1381397);

  case (doWhat) of
  crafting: smartImage.drawText('iKarahid Crafter', point(33, 479), LoginChars, true, 2075092);
  smelting: smartImage.drawText('iKarahid Smelter', point(33, 479), LoginChars, true, 2075092);
  cooking: smartImage.drawText('iKarahid Cooker', point(33, 479), LoginChars, true, 2075092);
  tanning: smartImage.drawText('iKarahid Tanner', point(33, 479), LoginChars, true, 2075092);
  end;

  smartImage.drawText('Time ran: ' + timeRunning() + '', point(45, 500), UpChars, true, 2075092);
  smartImage.drawText('Items made: ' + toStr(items) + ' (' + toStr(round(items2)) + '/hr)', point(45, 515), UpChars, true, 2075092);
  smartImage.drawText('Loads done: ' + toStr(loads) + ' (' + toStr(round(loads2)) + '/hr)', point(45, 530), UpChars, true, 2075092);
  smartImage.drawText('GP earned: ' + toStr(gp) + ' (' + toStr(round(gp2)) + '/hr)', point(45, 545), UpChars, true, 2075092);

  if (losingMoney) then
   smartImage.drawText('YOU ARE LOSING MONEY', point(45, 560), UpChars, true, clRed);

  smartImage.drawText('Status: ' + sstatus + '', point(320, 580), UpChars, true, clMaroon);
end;

procedure incrementVars();
begin
  case (doWhat) of
   crafting: items := (items + 28); //crafting will always have a full load of bars
   smelting:
   case (smeltWhat) of
    bronze: items := (items + 14);
    iron: items := (items + 28);
    steel: items := (items + 28);
   {mithril:
    adamant:
    rune:}
    cannonball: items := (items + 112);
   end;
   cooking: items := (items + 28); //cooking will always have a full load of food
   tanning: items := (items + 28); //tanning will always have a full load of hides
  end;

  inc(loads); //loads should be +1 no matter what
end;

function getLocation():TPoint;
var
  places: TPointArray;
  bankDist, furnaceDist:integer;
begin
  redraw();
  result := map.getPlayerPos();
  currentLocation := '';

  if result.equals([-1, -1]) then
   exit;

  furnaceDist := distance(result, point(95, 104));
  bankDist := distance(result, point(90, 164));

  if (furnaceDist < 15) then
   currentLocation := 'furnace'
  else
  if (bankDist < 15) then
   currentLocation := 'bank'
  else
   currentLocation := 'unknown';
end;

(*
  Overridden function. Walks a path of points, mousing off the client each time the minimap is clicked.
  Simulates a semi-afk player, making walking look a lot less bot-like.

  Also doubled the allowed distace from the flag before the next click. Improves accuracy.

  Example:
    sps.walkPath([point(69, 69), point(420, 420)]);
*)
function TSPSArea.walkPath(path: TPointArray): boolean; override;
var
  p, lastPos, mmPoint: TPoint;
  t, fails, h, l, i: integer;
begin
  result := false;;

  h := high(path);
  l := low(path);

  t := (getSystemTime() + randomRange(15000, 20000));

  repeat
    if (not isLoggedIn()) then
      exit(false);

    p := self.getPlayerPos();

    for i := h downto l do
      if (SPS_PosToMM(path[i], p, mmPoint)) then
      begin
        mouseBox(minimap.getBounds(), MOUSE_MOVE);
        if (distance(minimap.getCenterPoint(), mmPoint) >= 10) then //click it
        begin
            mouse(mmPoint, MOUSE_LEFT);
            mouseOffClient(OFF_CLIENT_TOP); //mouses off the client, top makes the most sense
                                            //because the minimap is near the top of the screen
          if (minimap.isFlagPresent(2500 + random(500))) then
            minimap.waitFlag(10 + random(25));
        end;

        t := (getSystemTime() + randomRange(15000, 20000));

        result := (i = h) or (distance(path[i], path[h]) < 10);

        if (result) then
          break(2)
        else
          break();

      end;

    if (p.x = lastPos.x) and (p.y = lastPos.y) then
      inc(fails);

    lastPos := p;

  until (getSystemTime() > t) or (fails > 5);

  if (minimap.isFlagPresent()) or (minimap.isPlayerMoving()) then
    minimap.waitPlayerMoving();

  print(self.getName()+'.walkPath(): result = '+boolToStr(result));
end;

function walk(where:string; bank:boolean):boolean;
var
  path:TPointArray;
  location:TPoint;
  threshold:integer;
  t:TTimeMarker;
begin
  sstatus := 'Handling mapwalking';
  redraw();
  location := getLocation;
  if location.equals([-1, -1]) then
   exit;

  if (where = 'furnace') then
   path := paths[0].copy()
  else if (where = 'range') then
   path := paths[1].copy()
  else if (where = 'tanner') then
   path := paths[2].copy()
  else if (where = 'init') then
   path := paths[3].copy()

  if (bank) then
   { if map.blindWalk(point(94, 163)) then }
   path := [Point(119, 271), Point(98, 284)]; //universal path, from anywhere to bank

  threshold := (getSystemTime() + randomRange(45000, 65000));

   writeDebug('Walking to ' + where);
   if map.walkPath(path) then
    minimap.waitPlayerMoving();
   wait(randomRange(250, 500));
   location := getLocation();

  result := (threshold > getSystemTime());
end;

function TPointArray.centerPoint():TPoint;
var
  tpa:TPointArray;
begin
  if (length(self) < 1) then
   exit;
  tpa := self;
  tpa.sortFromPoint(self.getMiddle());
  result := tpa[0];
end;

(*
  Hovers in the general area of the start button, but does not click.
  Meant to be called directly after clicking to open the production screen.
  Simulates a legit player preemptively hovering the location where she knows the button will appear.

  Uses static coordinates, so make sure the production screen is in the default location.

  Example:
    productionScreen.hoverStart();
*)
procedure TRSProductionScreen.hoverStart();
var
  b:TBox;
begin
  b := intToBox(268, 311, 526, 367);
  smartImage.drawBox(b, clRed);
  mouseBox(b, MOUSE_MOVE);
end;

function findFurnace():T2DPointArray;
var
  tpa:TPointArray;
begin
  mouseBox(mainScreen.getBounds(), MOUSE_MOVE);
  if findColorsTolerance(tpa, furnace.col, furnace.area, furnace.tol, furnace.cts) then
    result := tpa.toAtpa(30, 30); //split really big so we can effectively sort them by size later
  smartImage.debugAtpa(result);
  sortAtpaSize(result, true); //furnace colors match so much other stuff so sorting by size helps a lot
end;

function getFurnace():boolean;
var
  t2d:T2DPointArray;
  p:TPoint;
  i, h, timer:integer;
begin
  sstatus := 'Handling furnace';
  redraw();

  t2d := findFurnace();

  if (length(t2d) < 1) then
   exit;

  h := high(t2d);

  for i := 0 to h do
  begin
    if productionScreen.isOpen() then
     exit(true);

    if (length(t2d[i]) < 5) then
     continue;

    p := t2d[i].centerPoint();
    mouse(p.rand(-3, 3), MOUSE_MOVE, MOUSE_HUMAN);
    if isMouseOverText(['Smelt Furnace', 'melt Furnac', 'lt Fur'], randomRange(500, 1000)) then
     fastClick(MOUSE_LEFT);
     begin
       //productionScreen.hoverStart(); TODO fix this
       minimap.isFlagPresent(randomRange(4000, 7000)); //faster than minimap.waitplayermoving
       timer := (getSystemTime() + randomRange(3000, 6000));
       repeat
         wait(randomRange(100, 400));
       until (productionScreen.isOpen()) or (getSystemTime() > timer);
     end;
  end;
end;

function handleFurnace():boolean;
var
  box:integer;
begin
  sstatus := 'Handling interfaces';
  redraw();
  if not productionScreen.isOpen() then
   exit(false);

  if (doWhat = crafting) then
   begin
     if (craftWhat = necklace) then
      box := 2
     else
      box := 3;
   end;

  if not (box = auto) then
   productionScreen.selectBox(box);

  result := productionScreen.clickStart(false);
  incrementVars();
end;

function findRange():T2DPointArray;
var
  tpa:TPointArray;
begin
  mouseBox(mainScreen.getBounds(), MOUSE_MOVE);
  if findColorsTolerance(tpa, furnace.col, furnace.area, furnace.tol, furnace.cts) then
    result := tpa.toAtpa(30, 30); //split really big so we can effectively sort them by size later
  smartImage.debugAtpa(result);
  sortAtpaSize(result, true); //furnace colors match so much other stuff so sorting by size helps a lot
end;

function getRange():boolean;
var
  t2d:T2DPointArray;
  p:TPoint;
  i, h, timer:integer;
begin
  sstatus := 'Handling range';
  redraw();

  t2d := findRange();

  if (length(t2d) < 1) then
   exit;

  h := high(t2d);

  for i := 0 to h do
  begin
    if productionScreen.isOpen() then
     exit(true);

    if (length(t2d[i]) < 5) then
     continue;

    p := t2d[i].centerPoint();
    mouse(p.rand(-3, 3), MOUSE_MOVE, MOUSE_HUMAN);
    if isMouseOverText(['Smelt Furnace', 'melt Furnac', 'lt Fur'], randomRange(500, 1000)) then
     fastClick(MOUSE_LEFT);
     begin
       //productionScreen.hoverStart(); TODO fix this
       minimap.isFlagPresent(randomRange(4000, 7000)); //faster than minimap.waitPlayerMoving()
       timer := (getSystemTime() + randomRange(3000, 6000));
       repeat
         wait(randomRange(100, 400));
       until (productionScreen.isOpen()) or (getSystemTime() > timer);
     end;
  end;
end;

function handleRange():boolean;
var
  box:integer;
begin
  sstatus := 'Handling interfaces';
  redraw();
  if not productionScreen.isOpen() then
   exit(false);

  if not (box = auto) then
   productionScreen.selectBox(box);

  result := productionScreen.clickStart(false);
  incrementVars();
end;

function findTanner():T2DPointArray;
var
  tpa:TPointArray;
begin
  mouseBox(mainScreen.getBounds(), MOUSE_MOVE);
  if findColorsTolerance(tpa, furnace.col, furnace.area, furnace.tol, furnace.cts) then
    result := tpa.toAtpa(30, 30); //split really big so we can effectively sort them by size later
  smartImage.debugAtpa(result);
  sortAtpaSize(result, true); //furnace colors match so much other stuff so sorting by size helps a lot
end;

function getTanner():boolean;
var
  t2d:T2DPointArray;
  p:TPoint;
  i, h, timer:integer;
begin
  sstatus := 'Handling range';
  redraw();

  t2d := findRange();

  if (length(t2d) < 1) then
   exit;

  h := high(t2d);

  for i := 0 to h do
  begin
    if productionScreen.isOpen() then
     exit(true);

    if (length(t2d[i]) < 5) then
     continue;

    p := t2d[i].centerPoint();
    mouse(p.rand(-3, 3), MOUSE_MOVE, MOUSE_HUMAN);
    if isMouseOverText(['Smelt Furnace', 'melt Furnac', 'lt Fur'], randomRange(500, 1000)) then
     fastClick(MOUSE_LEFT);
     begin
       //productionScreen.hoverStart(); TODO fix this
       minimap.isFlagPresent(randomRange(4000, 7000)); //faster than minimap.waitplayermoving
       timer := (getSystemTime() + randomRange(3000, 6000));
       repeat
         wait(randomRange(100, 400));
       until (productionScreen.isOpen()) or (getSystemTime() > timer);
     end;
  end;
end;

function handleTanner():boolean;
var
  box:integer;
begin
  sstatus := 'Handling interfaces';
  redraw();
  if not productionScreen.isOpen() then
   exit(false);

  if not (box = auto) then
   productionScreen.selectBox(box);

  result := productionScreen.clickStart(false);
  incrementVars();
end;

function doneButton():boolean;
var
  tpa:TPointArray;
  b:TBox;
begin
  b := [244, 197, 335, 223];
  findColorsTolerance(tpa, 13278759, b, 44);
  result := (length(tpa) > 145);
end;

procedure waitProgressScreen();
var
  count, threshold:integer;
begin
  sstatus := 'Waiting';
  redraw();
  threshold := (getSystemTime() + randomRange(70000, 80000));
  mouseOffClient(OFF_CLIENT_RANDOM); //joe's suggested antiban
  repeat
   wait(randomRange(255, 555));
   if (doneButton()) then
    begin
      threshold := (getSystemTime() + randomRange(2500, 3500));
      repeat
       wait(randomRange(255, 555));
       if not (doneButton) then
        exit;
      until (getSystemTime() > threshold);
    end;
  until (getSystemTime() > threshold);
end;

(*
  Hovers in the general area of the quick deposit buttons, but does not click.
  Meant to be called directly after clicking to open the bank.
  Simulates a legit player preemptively hovering the location where she knows the buttons will appear.

  Uses static coordinates, so make sure the bankscreen is in the default location.

  Example:
    bankScreen.hoverDeposit();
*)
procedure TRSBankScreen.hoverDeposit();
var
  b:TBox;
begin
  b := intToBox(337, 533, 576, 593);
  smartImage.drawBox(b, clRed);
  mouseBox(b, MOUSE_MOVE);
end;

function findBank():T2DPointArray;
var
  tpa:TPointArray;
begin
  mouseBox(mainScreen.getBounds(), MOUSE_MOVE);
  if findColorsTolerance(tpa, bankers.col, bankers.area, bankers.tol, bankers.cts) then
   result := tpa.toAtpa(15, 15);
  smartImage.debugAtpa(result);
  sortAtpaFromMidPoint(result, mainscreen.playerPoint);
end;

function getBank():boolean;
var
  t2d:T2DPointArray;
  p:TPoint;
  i, h, threshold:integer;
  t:TTimeMarker;
begin
  sstatus := 'Handling bank';
  redraw();

  if bankscreen.isOpen() then
   exit(true);

  t2d := findBank();

  if (length(t2d) < 1) then
   exit;

  h := high(t2d);

  for i := 0 to h do
   begin
     if (length(t2d[i]) < 5) then
      continue;

     p := t2d[i].centerPoint();
     mouse(p.rand(-3, 3), MOUSE_MOVE, MOUSE_HUMAN);

     if isMouseOverText(['Bank Banker', 'ank Bank', 'nk Ban'], randomRange(500, 1000)) then
      fastClick(MOUSE_LEFT);
      begin
        bankScreen.hoverDeposit();
        threshold := (randomRange(3000, 6000));
        t.start();

        repeat
         wait(randomRange(100, 400));
        until (bankScreen.isOpen()) or (pinScreen.isOpen()) or (t.getTime() > threshold);

        if pinScreen.isOpen() then
         pinScreen.enter(bankPin);

        if bankscreen.isOpen() then
         exit(true);
      end;
  end;
end;

function handleBank_crafting():boolean;
begin
  if waitFunc(@getBank, 100, randomRange(20000, 30000)) then
   begin
     sstatus := 'Handling interfaces';
     redraw();
     bankScreen.quickDeposit(QUICK_DEPOSIT_INVENTORY);
     if not bankScreen.withdraw(goldBarDtm, 28) then
      begin
        writeError('Out of supplies, terminating');
        terminateScript();
      end;
     if (craftWhat = necklace) then
      result := (bankScreen.getPackCountDtm(goldNecklaceDtm) < 1)
     else
      result := (bankScreen.getPackCountDtm(goldBraceletDtm) < 1);

     if (result) then
      bankScreen.close()
     else
      handleBank_crafting();
   end;
end;

function handleBank_smelting():boolean;
var
  primaryAmount, secondaryAmount:integer;
begin
  if waitFunc(@getBank, 100, randomRange(20000, 30000)) then
   begin
     sstatus := 'Handling interfaces';
     redraw();
     bankScreen.quickDeposit(QUICK_DEPOSIT_INVENTORY);

  case (smeltWhat) of
  bronze:
  begin
    primaryAmount := 14;
    secondaryAmount := 14;
  end;
  iron:
  begin
    primaryAmount := 28;
    secondaryAmount := 0;
  end;
  steel:
  begin
    primaryAmount := 9;
    secondaryAmount := 18;
  end;
  mithril:
  begin
    primaryAmount := 5;
    secondaryAmount := 23;
  end;
  adamant:
  begin
    primaryAmount := 3;
    secondaryAmount := 25;
  end;
  rune:
  begin
    primaryAmount := 3;
    secondaryAmount := 25;
  end;
  cannonball:
  begin
    primaryAmount := 28;
    secondaryAmount := 0;
  end;
  end;

  bankScreen.withdraw(slot1, primaryAmount, ['']);

  if (secondaryAmount > 1) then
   bankScreen.withdraw(slot2, secondaryAmount, ['']);

  result := (bankScreen.getPackCountDtm(genericBarDtm) < 1);

     if (result) then
      bankScreen.close()
     else
      handleBank_smelting();
   end;
end;

function handleBank_cooking():boolean;
begin
  if waitFunc(@getBank, 100, randomRange(20000, 30000)) then
   begin
     sstatus := 'Handling interfaces';
     redraw();
     bankScreen.quickDeposit(QUICK_DEPOSIT_INVENTORY);
     if not bankScreen.withdraw(slot1, WITHDRAW_AMOUNT_ALL, ['']) then
      begin
        writeError('Out of supplies, terminating');
        terminateScript();
      end;
     if (craftWhat = necklace) then
      result := bankScreen.getPackCountDtm(goldNecklaceDtm) < 1
     else
      result := bankScreen.getPackCountDtm(goldBraceletDtm) < 1

     if (result) then
      bankScreen.close()
     else
      handleBank_crafting();
   end;
end;

function handleBank_tanning():boolean;
begin
  if waitFunc(@getBank, 100, randomRange(20000, 30000)) then
   begin
     sstatus := 'Handling interfaces';
     redraw();
     bankScreen.quickDeposit(QUICK_DEPOSIT_INVENTORY);
     if not bankScreen.withdraw(slot1, WITHDRAW_AMOUNT_ALL, ['']) then
      begin
        writeError('Out of supplies, terminating');
        terminateScript();
      end;
     if craftWhat = necklace then
      result := bankScreen.getPackCountDtm(goldNecklaceDtm) < 1
     else
      result := bankScreen.getPackCountDtm(goldBraceletDtm) < 1

     if (result) then
      bankScreen.close()
     else
      handleBank_crafting();
   end;
end;

procedure craftingLoop();
var
 location:TPoint;
begin
  redraw();
  location := getLocation();
  if location.equals([-1, -1]) then
   exit;

  wait(randomRange(1, 300));

  if walk('bank', true) then
   if getBank() then
    if handleBank_crafting() then
     if walk('furnace', false) then
      if getFurnace() then
       if handleFurnace() then
        waitProgressScreen();
end;

procedure smeltingLoop();
var
 location:TPoint;
begin
  redraw();
  location := getLocation();
  if location.equals([-1, -1]) then
   exit;

  wait(randomRange(1, 300));

  if walk('bank', true) then
   if getBank() then
    if handleBank_smelting() then
     if walk('furnace', false) then
      if getFurnace() then
       if handleFurnace() then
        waitProgressScreen();
end;

procedure cookingLoop();
var
 location:TPoint;
begin
  redraw();
  location := getLocation();
  if location.equals([-1, -1]) then
   exit;

  wait(randomRange(1, 300));

  if walk('bank', true) then
   if getBank() then
    if handleBank_cooking() then
     if walk('range', false) then
      if getRange() then
       if handleRange() then
        waitProgressScreen();
end;

procedure tanningLoop();
var
 location:TPoint;
begin
  redraw();
  location := getLocation();
  if location.equals([-1, -1]) then
   exit;

  wait(randomRange(1, 300));

  if walk('bank', true) then
   if getBank() then
    if handleBank_tanning() then
     if walk('tanner', false) then
      if getTanner() then
       if handleTanner() then
        waitProgressScreen();
end;

procedure init();
var
  location:TPoint;
begin
  redraw();
  location := getLocation();
  if location.equals([-1, -1]) then
   begin
     writeWarn('Player in not in the correct area, attempting to autocorrect');
     lodestoneScreen.teleportTo(LOCATION_AL_KHARID);
     wait(30000);
     walk('init', false);
   end;
end;

procedure everything();
var
  location:TPoint;
begin
  redraw();

  if (not isLoggedIn()) then
  begin
    writeWarn('Player is not logged in. Respawning SMART client.');
    wait(randomRange(5000, 10000));
    smartReloadClient((5 * 60000) + random(15000));

    if (waitClientReady()) then
    begin
      writeDebug('SMART successfully respawned.');
    end else
    begin
      writeError('SMART failed to respawn.');
      terminateScript();
    end;

    if players[currentPlayer].login() then
     initPlayer();

    if (not isLoggedIn()) then
    begin
      writeError('Failed to log in. Respawning SMART client.');
      everything();
    end;
  end;

  location := getLocation();
  if location.equals([-1, -1]) then
   init();

  case (doWhat) of
    crafting: craftingLoop();
    smelting: smeltingLoop();
    cooking: cookingLoop();
    tanning: tanningLoop();
  end;
end;

begin
  verify();
  initScript();
  initPrices();
  initPlayer();
  initColors();
  initDtms();
  initPaths();

  if not isLoggedIn() then
   if players[currentPlayer].login() then
    initPlayer();

  while (players.getActive > 0) do
   everything();
end.
